import Promise from 'bluebird';
import getProductLinkDestinations from './functions/get-product-link-destinations';
import buildProductModel from './functions/build-product-model';

export default function (axios) {
  const getProductPagePromises = (response) => {
    if (response.status !== 200) {
      return Promise.reject('Product listing page gave a non 200 response');
    }

    return getProductLinkDestinations(response.data).map(link => axios.get(link));
  };

  const convertToKB = bytes => `${(bytes / 1024).toFixed(1).toString()}kb`;

  const scrapeProduct = (productResponse) => {
    const productModel = buildProductModel(productResponse.data);
    productModel.size = convertToKB(productResponse.headers['content-length']);
    return productModel;
  };

  const scrapeProducts = productPages => productPages.map(scrapeProduct);
  const productTotal = products => products
    .reduce((prev, curr) => prev + parseFloat(curr.unit_price), 0);

  return axios.get('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html')
    .then(getProductPagePromises)
    .then(axios.all)
    .then(scrapeProducts)
    .then(products => JSON.stringify(
      { results: products, total: productTotal(products).toFixed(2) },
      null,
      2
    )
  );
}
