import cheerio from 'cheerio';

export default (html) => {
  const $ = cheerio.load(html);

  const getTitle = () => $('.productTitleDescriptionContainer')
    .text()
    .trim();

  const getUnitPrice = () => $('.pricePerUnit')
    .first()
    .text()
    .replace(/(£|\/|unit)/gi, '')
    .trim();

  const getDescription = () => $('.section#information')
    .find('.productText')
    .first()
    .text()
    .trim();

  return {
    title: getTitle(),
    unit_price: getUnitPrice(),
    description: getDescription(),
  };
};
