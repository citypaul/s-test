import cheerio from 'cheerio';

export default (html) => {
  const $ = cheerio.load(html);
  return $('.productInfo h3 a').map((index, element) => $(element).attr('href')).get();
};
