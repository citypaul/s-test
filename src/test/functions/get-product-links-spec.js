import fs from 'fs';
import { expect } from 'chai';
import productLinks from '../../app/functions/get-product-link-destinations';

function getMockFile(fileName) {
  return fs.readFileSync(`${__dirname}/../mocks/products/${fileName}`, 'utf-8');
}

describe('Product links from the product listing page', () => {
  it('should return all the product link destinations on the page', () => {
    const expectedLinks = [
      'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g.html',
      'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-avocado-xl-pinkerton-loose-300g.html',
    ];
    const mockHtml = getMockFile('product-index-2-links.html');

    expect(productLinks(mockHtml)).to.eql(expectedLinks);
  });

  it('should return an empty array if no product links are found on the page', () => {
    const mockHtml = getMockFile('product-index-no-products.html');
    expect(productLinks(mockHtml)).to.eql([]);
  });
});
