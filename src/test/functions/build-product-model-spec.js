import fs from 'fs';
import { expect } from 'chai';
import productModel from '../../app/functions/build-product-model';

function getMockFile(fileName) {
  return fs.readFileSync(`${__dirname}/../mocks/products/${fileName}`, 'utf-8');
}

describe('Product links from the product listing page', () => {
  describe('title', () => {
    it('should return a title if one exists', () => {
      const result = productModel(getMockFile('avocados.html'));
      expect(result.title).to.equal("Sainsbury's Avocado, Ripe & Ready x2");
    });

    it('should return an empty string if no title exists', () => {
      const result = productModel(getMockFile('bad-data/product-missing-title.html'));
      expect(result.title).to.equal('');
    });
  });

  describe('Unit price', () => {
    it('should return a unit price if one exists', () => {
      const result = productModel(getMockFile('avocados.html'));
      expect(result.unit_price).to.equal('1.80');
    });
  });

  describe('description', () => {
    it('should return a description', () => {
      const result = productModel(getMockFile('avocados.html'));
      expect(result.description).to.equal('Avocados');
    });
  });
});
