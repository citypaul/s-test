import mockAxios from 'axios';
import Promise from 'bluebird';
import sinon from 'sinon';
import fs from 'fs';
import { expect } from 'chai';
import scrapeProducts from '../app/scrape-products';

function getMockFile(fileName) {
  return fs.readFileSync(`${__dirname}/mocks/products/${fileName}`, 'utf-8');
}

beforeEach(() => {
  mockAxios.get = sinon.stub();
});

function mockAxiosGetRequest(httpEndpoint, fileName, size = 10000, status = 200) {
  const data = getMockFile(fileName);

  mockAxios.get.withArgs(
    httpEndpoint
  ).returns(
    Promise.resolve({
      data,
      headers: {
        'content-length': size,
      },
      status,
    }
    )
  );
}

describe('Product scraper', () => {
  it('return the expected product models after scraping the product page', (done) => {
    const expectedResult = {
      results: [
        {
          title: 'Sainsbury\'s Apricot Ripe & Ready x5',
          unit_price: '3.50',
          description: 'Apricots',
          size: '34.2kb',
        },
        {
          title: 'Sainsbury\'s Avocado, Ripe & Ready x2',
          unit_price: '1.80',
          description: 'Avocados',
          size: '48.8kb',
        },
      ],
      total: '5.30',
    };

    mockAxiosGetRequest('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html', 'product-index-2-links.html');
    mockAxiosGetRequest('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-apricot-ripe---ready-320g.html', 'apricots.html', 35000);
    mockAxiosGetRequest('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/sainsburys-avocado-xl-pinkerton-loose-300g.html', 'avocados.html', 50000);

    scrapeProducts(mockAxios).then((result) => {
      expect(JSON.parse(result)).to.deep.equal(expectedResult);
      done();
    }).catch(done);
  });

  it('should propagate errors if no 200 response is received for the product listing', (done) => {
    mockAxiosGetRequest('http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html', 'product-index-2-links.html', 5000, 400);
    scrapeProducts(mockAxios).then(() => {

    }).catch((err) => {
      expect(err).to.equal('Product listing page gave a non 200 response');
      done();
    });
  });
});
