module.exports = function (wallaby) {
    return {
        files: [
            'src/app/**/*.js',
            'src/test/mocks/**/*'
        ],

        tests: [
            'src/test/**/*spec.js'
        ],
        compilers: {
            '**/*.js': wallaby.compilers.babel()
        },
        env: {
            type: 'node'
        }
    };
};
