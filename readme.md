##Installation

If you are using `nvm`, please run `nvm install && nvm use` first. This will use the included `.nvmrc` file to ensure you are using Node v4.5.0.

To install the dependencies, please run `npm install`.

##Running

You can run the code by running: `npm start`

##Tests

The tests will run via: `npm test`. This will also run Eslint (which can be ran separately  with `npm run lint`).

##Notes

I have used Axios for the get requests. Axios is a promise based http library for Node, and using it meant I could take advantage of promises without having to do any work around wrapping Node's http libraries.

###Notes on tests
I did employ a test first strategy, although I also did a small spike before starting out for real in order to prove I could mock out the calls made by Axios via Sinon.

There are numerous ways that this problem could be solved, and numerous ways any individual implementation could be tested. I considered using [Nock](https://github.com/node-nock/nock) at first, but after considering my options I realised I'd be able to simply inject Axios into the scraper as a parameter, which would allow me to then mock out any get requests via Sinon, and therefore would be able to unit test without the need for Nock.

I believe in general that tests should serve two primary purposes:

* Give a high confidence that the current code works
* Make change easier to deal with - whether by adding new functionality or refactoring

I considered a few options regarding finding the best "level" for where I wanted to test. I considered initially testing everything at the scraper level itself, because one perspective would be that any functions used internally by the scraper are simply implementation details, and as such I could test the output only. This would leave me free to move functions around and refactor at will (although it may look a little strange to someone who is used to seeing every exported function explicitly tested).

I decided in the end to do a combination of testing exported functions and testing the scraper directly (via mocking out Axios). The reason for this is because the functions I've created could in theory be used outside of the context of this scraper if we were in a larger application, and therefore it makes sense to test them directly as in a real application where such code could be reused, such tests would lock down the functionality we care about now while hopefully making it easier for other people to add/change in the future.

I believe the best answer to these kind of questions depends upon a wider understanding of the system itself and even the preferences of the team as a whole, so the tests I'd write in a real system may be somewhat different.

I think they serve the purpose now though, and you should be able to prove it by trying to break my code anywhere and running the tests - they should fail as expected, and at the same time the tests should support refactoring and new features. I do think they meet these requirements at this point in time.


###Other notes

I'm using ES6 with Babel producing the `dist` folder after running `npm run build`. I've not used ES6 much in production but decided to use it here as I'd prefer to use it in future work.

I'm using Eslint with the auto generated rules (`eslint --init`) for `airbnb`.  I don't have a strong preference for Eslint rules - I'm generally of the opinion that the most important factor is that the team has an agreed upon ruleset and that they are enforced by a linting tool.

I did quite enjoy the feedback I was getting from the `airbnb` ruleset - it's partly due to this ruleset that I was able to take advantage of so much ES6, as it continually either forced me to, or recommended, adopting ES6 replacements for ES5 syntax.


I've added an editor config file to ensure a consistent indentation size and style, as well as enforcing unix style newlines with a newline ending every file. This is picked up by my editor by default, and is picked up by other editors in the same way, with some requiring a plugin. I consider it a nicety.

Finally I also added an `.nvmrc` file to ensure people using `nvm` can flip over to the version of Node I used, which in this case is `4.5.0`.

##Missing/Unfinished
There's actually no test for the index.js file itself at this time. I think in a real application I'd probably want an end to end style test around the final file itself. I'm happy that the scraper itself is fully covered, but the index.js file itself in a real application would be tested in an end to end style, or some equivalent depending on the technical architecture used.

I had an issue getting the floating point number to come out as a number type in the JSON - I seemed to only be able to get numbers like 1.50  to come out as either the number 1.5 or the string "1.50". Because the example data displayed 1.5 as "1.50", I'm storing this information as a string at the moment. Ideally currency should be a data type that also holds information about the type of currency, eg GBP.


